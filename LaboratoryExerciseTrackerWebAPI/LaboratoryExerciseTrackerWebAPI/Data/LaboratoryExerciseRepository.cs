﻿using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Data
{
    public class LaboratoryExerciseRepository : IRepository<LaboratoryExercise>
    {
        private readonly LabContext _context = null;

        public LaboratoryExerciseRepository(IOptions<Settings> settings)
        {
            _context = new LabContext(settings);
        }

        public async Task Add(LaboratoryExercise item)
        {
            try
            {
                await _context.LaboratoryExercise.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<LaboratoryExercise> Get(string id)
        {
            var filter = Builders<LaboratoryExercise>.Filter.Eq("Id", id);
            

            try
            {
                return await _context.LaboratoryExercise
                                     .Find(filter)
                                     .FirstOrDefaultAsync(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<LaboratoryExercise>> GetAll()
        {
            try
            {
                return await _context.LaboratoryExercise.Find(_ => true).ToListAsync(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.LaboratoryExercise.DeleteOneAsync(Builders<LaboratoryExercise>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult = await _context.LaboratoryExercise.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(string id, LaboratoryExercise item)
        {
            try
            {//change professor id
                ReplaceOneResult actionResult = await _context.LaboratoryExercise.ReplaceOneAsync(n => n.Id.Equals(id), item, new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
