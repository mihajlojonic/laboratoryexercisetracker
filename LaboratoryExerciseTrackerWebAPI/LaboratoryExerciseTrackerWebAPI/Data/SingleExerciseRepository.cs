﻿using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Data
{
    public class SingleExerciseRepository : IRepository<SingleExercise>
    {
        private readonly LabContext _context = null;
        
        public SingleExerciseRepository(IOptions<Settings> settings)
        {
            _context = new LabContext(settings);
        }

        public async Task Add(SingleExercise item)
        {
            try
            {
                await _context.SingleExercise.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<SingleExercise> Get(string id)
        {
            var filter = Builders<SingleExercise>.Filter.Eq("Id", id);

            try
            {
                return await _context.SingleExercise
                                     .Find(filter)
                                     .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<SingleExercise>> GetAll()
        {
            try
            {
                return await _context.SingleExercise.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.SingleExercise.DeleteOneAsync(Builders<SingleExercise>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult = await _context.SingleExercise.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(string id, SingleExercise item)
        {
            try
            {//change comment
                ReplaceOneResult actionResult = await _context.SingleExercise.ReplaceOneAsync(n => n.Id.Equals(id), item, new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
