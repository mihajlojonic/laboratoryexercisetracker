﻿using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Data
{
    public class ProfessorRepository : IRepository<Professor>
    {
        private readonly LabContext _context = null;

        public ProfessorRepository(IOptions<Settings> settings)
        {
            _context = new LabContext(settings);
        }

        public async Task Add(Professor item)
        {
            try
            {
                await _context.Professors.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Professor> Get(string id)
        {
            var filter = Builders<Professor>.Filter.Eq("Id", id);

            try
            {
                return await _context.Professors
                                     .Find(filter)
                                     .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Professor>> GetAll()
        {
            try
            {
                return await _context.Professors.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.Professors.DeleteOneAsync(Builders<Professor>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult = await _context.Professors.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(string id, Professor item)
        {
            try
            {
                ReplaceOneResult actionResult = await _context.Professors.ReplaceOneAsync(n => n.Id.Equals(id), item, new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateSubjectsIds(string id, IEnumerable<string> subjectsIds)
        {
            var filter = Builders<Professor>.Filter.Eq(s => s.Id, id);
            var update = Builders<Professor>.Update.Set(s => s.SubjectsIds, subjectsIds);

            try
            {
                UpdateResult actionResult = await _context.Professors.UpdateOneAsync(filter, update);

                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateLaboratoryIds(string id, IEnumerable<string> laboratoryIds)
        {
            var filter = Builders<Professor>.Filter.Eq(s => s.Id, id);
            var update = Builders<Professor>.Update.Set(s => s.LaboratoryIds, laboratoryIds);

            try
            {
                UpdateResult actionResult = await _context.Professors.UpdateOneAsync(filter, update);

                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
