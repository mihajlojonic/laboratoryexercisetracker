﻿using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Data
{
    public class SubjectRepository : IRepository<Subject>
    {
        private readonly LabContext _context = null;

        public SubjectRepository(IOptions<Settings> settings)
        {
            _context = new LabContext(settings);
        }

        public async Task Add(Subject item)
        {
            try
            {
                await _context.Subjects.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Subject> Get(string id)
        {
            var filter = Builders<Subject>.Filter.Eq("Id", id);

            try
            {
                return await _context.Subjects
                                     .Find(filter)
                                     .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public async Task<List<Subject>> GetAll()
        {
            try
            {
                return await _context.Subjects.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.Subjects.DeleteOneAsync(Builders<Subject>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult = await _context.Subjects.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(string id, IDictionary<string, string> professors)
        {
            var filter = Builders<Subject>.Filter.Eq(s => s.Id, id);
            var update = Builders<Subject>.Update.Set(s => s.Professors, professors);

            try
            {
                UpdateResult actionResult = await _context.Subjects.UpdateOneAsync(filter, update);

                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(string id, Subject item)
        {
            try
            {
                ReplaceOneResult actionResult = await _context.Subjects.ReplaceOneAsync(n => n.Id.Equals(id), item, new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
