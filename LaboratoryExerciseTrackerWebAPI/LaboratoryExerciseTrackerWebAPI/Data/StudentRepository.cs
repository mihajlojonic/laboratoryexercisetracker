﻿using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Data
{
    public class StudentRepository : IRepository<Student>
    {
        private readonly LabContext _context = null;

        public StudentRepository(IOptions<Settings> settings)
        {
            _context = new LabContext(settings);
        }

        public async Task Add(Student item)
        {
            //var insert = Builders<List<LaboratoryExercise>>.Update.Push("Student.$[].ListOfLaboratoryExercise",item.ListOfLaboratoryExercises);
            try
            {

                await _context.Students.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Student> Get(string id)
        {
            var filter = Builders<Student>.Filter.Eq("Id", id);

            try
            {
                return await _context.Students
                                     .Find(filter)
                                     .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Student>> GetAll()
        {
            try
            {
                return await _context.Students.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.Students.DeleteOneAsync(Builders<Student>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult = await _context.Students.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(string id, Student item)
        {
            try
            {
                ReplaceOneResult actionResult = await _context.Students.ReplaceOneAsync(n => n.BrInd.Equals(id), item, new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
