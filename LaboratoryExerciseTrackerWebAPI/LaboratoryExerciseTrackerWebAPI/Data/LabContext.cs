﻿using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Data
{
    public class LabContext
    {
        private readonly IMongoDatabase _database = null;

        public LabContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Subject> Subjects
        {
            get
            {
                return _database.GetCollection<Subject>("Subject");
            }
        }

        public IMongoCollection<Professor> Professors
        {
            get
            {
                return _database.GetCollection<Professor>("Professor");
            }
        }

        public IMongoCollection<Student> Students
        {
            get
            {
                return _database.GetCollection<Student>("Student");
            }
        }

        public IMongoCollection<LaboratoryExercise> LaboratoryExercise
        {
            get
            {
                return _database.GetCollection<LaboratoryExercise>("LabaratoryExercise");
            }
        }

        public IMongoCollection<SingleExercise> SingleExercise
        {
            get
            {
                return _database.GetCollection<SingleExercise>("SingleExercise");
            }
        }
    }
}
