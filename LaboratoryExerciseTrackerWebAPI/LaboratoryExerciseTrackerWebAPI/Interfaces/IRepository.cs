﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<List<T>> GetAll();

        Task<T> Get(string id);

        Task Add(T item);

        Task<bool> Remove(string id);

        Task<bool> Update(string id, T item);

        Task<bool> RemoveAll();
    }
}
