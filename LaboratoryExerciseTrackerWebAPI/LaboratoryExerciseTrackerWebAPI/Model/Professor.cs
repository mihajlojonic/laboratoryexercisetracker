﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Model
{
    public class Professor
    {
        [BsonId]
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> SubjectsIds { get; set; }
        public List<string> LaboratoryIds { get; set; }
    }
}
