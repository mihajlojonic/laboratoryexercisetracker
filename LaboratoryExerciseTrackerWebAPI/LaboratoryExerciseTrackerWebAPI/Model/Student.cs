﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Model
{
    public class Student
    {
        [BsonId]
        public string BrInd { get; set; }
        public string Name { get; set; }
        public List<string> ListOfLaboratoryExercises { get; set; } //string is object id string od laboratori exercises
    }
}
