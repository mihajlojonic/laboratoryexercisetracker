﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaboratoryExerciseTrackerWebAPI.Model
{
    public class LaboratoryExercise
    {
        [BsonId]
        public string Id { get; set; }
        public string studentId { get; set; }
        public string subjectId { get; set; }
        public string ProfessorId { get; set; }
        public char Group { get; set; }
        public List<string> ListOfSingleExercise { get; set; }//string is object id string of single exercise
    }
}
