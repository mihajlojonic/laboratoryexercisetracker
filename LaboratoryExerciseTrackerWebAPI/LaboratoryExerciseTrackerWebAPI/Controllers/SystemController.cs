using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LaboratoryExerciseTrackerWebAPI.Data;
using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using MongoDB.Bson;
using System.IO;

namespace LaboratoryExerciseTrackerWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class SystemController : Controller
    {
        private readonly IRepository<Subject> _subjectRepository;
        private readonly IRepository<Professor> _professorRepository;
        private readonly IRepository<Student> _studentRepository;
        private readonly IRepository<LaboratoryExercise> _laboratoryExerciseRepository;
        private readonly IRepository<SingleExercise> _singleExerciseRepository;

        private List<Subject> listOfSubjects = new List<Subject>();
        private List<Professor> listOfProfessors = new List<Professor>();

        public SystemController(IRepository<Subject> subjectRepository, IRepository<Professor> professorRepository,
                                IRepository<Student> studentRepository, IRepository<LaboratoryExercise> laboratoryExercise,
                                IRepository<SingleExercise> singleExercise)
        {
            _subjectRepository = subjectRepository;
            _professorRepository = professorRepository;
            _studentRepository = studentRepository;
            _laboratoryExerciseRepository = laboratoryExercise;
            _singleExerciseRepository = singleExercise;
        }

        // Call an initialization - api/system/intiSubjects
        [HttpGet("{setting}")]
        public string Get(string setting)
        {
            String returnString = "";
            if (setting == "init")
            {
                _subjectRepository.RemoveAll();
                _professorRepository.RemoveAll();
                _studentRepository.RemoveAll();
                _laboratoryExerciseRepository.RemoveAll();
                _singleExerciseRepository.RemoveAll();

                returnString += "* Items removed \n";
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////CREATE OBJECTS//////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //////////////
                //PROFESSORS// - INIT
                //////////////
                Professor leonidProf = new Professor() { Id = ObjectId.GenerateNewId().ToString(), Name = "Leonid Stoimenov", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                Professor AStanimirovicProg = new Professor { Id = ObjectId.GenerateNewId().ToString(), Name = "Aleksandar Stanimirovic", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                Professor MilosBogdanovicProf = new Professor() { Id = ObjectId.GenerateNewId().ToString(), Name = "Milos Bogdanovic", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                Professor vladanProf = new Professor() { Id = ObjectId.GenerateNewId().ToString(), Name = "Vladan Mihajlovic", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                Professor rajkoProf = new Professor() { Id = ObjectId.GenerateNewId().ToString(), Name = "Petar Rajkovic", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                Professor vuckoProf = new Professor() { Id = ObjectId.GenerateNewId().ToString(), Name = "Vladan Vuckovic", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                Professor aksProf = new Professor() { Id = ObjectId.GenerateNewId().ToString(), Name = "Aleksandar Dimitrijevic", SubjectsIds = new List<string>(), LaboratoryIds = new List<string>() };
                
                returnString += "* Professors - inti - Done! \n";
                ////////////
                //SUBJECTS// - INIT and ADD professors
                ////////////
                Subject advancedDatebases = new Subject() { Id = ObjectId.GenerateNewId().ToString(), Name = "Advanced Datebases", Professors = new Dictionary<string, string>()
                {
                    { AStanimirovicProg.Name, AStanimirovicProg.Id }, { MilosBogdanovicProf.Name, MilosBogdanovicProf.Id }, { leonidProf.Name, leonidProf.Id }
                } };
                Subject artificialIntelligence = new Subject() { Id = ObjectId.GenerateNewId().ToString(), Name = "Artificial Intelligence", Professors = new Dictionary<string, string>()
                {
                    { leonidProf.Name, leonidProf.Id }, { vladanProf.Name, vladanProf.Id }, { aksProf.Name, aksProf.Id }
                } };
                Subject informationSecurity = new Subject() { Id = ObjectId.GenerateNewId().ToString(), Name = "Information Security", Professors = new Dictionary<string, string>()
                {
                    { vuckoProf.Name, vuckoProf.Id }, { rajkoProf.Name, rajkoProf.Id }
                } };
                returnString += "* Subjects - inti and add professors - Done! \n";
                //////////////
                //PROFESSORS// - ADD subjects
                //////////////

                (leonidProf.SubjectsIds as List<string>).Add(advancedDatebases.Id);
                (leonidProf.SubjectsIds as List<string>).Add(artificialIntelligence.Id);
                (AStanimirovicProg.SubjectsIds as List<string>).Add(advancedDatebases.Id);
                (MilosBogdanovicProf.SubjectsIds as List<string>).Add(advancedDatebases.Id);
                (vladanProf.SubjectsIds as List<string>).Add(artificialIntelligence.Id);
                (rajkoProf.SubjectsIds as List<string>).Add(informationSecurity.Id);
                (vuckoProf.SubjectsIds as List<string>).Add(informationSecurity.Id);
                (aksProf.SubjectsIds as List<string>).Add(artificialIntelligence.Id);
                returnString += "* Professors - add subjects - Done! \n";


                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////ADD TO DATEBASE/////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //ADD subjects
                _subjectRepository.Add(advancedDatebases);
                _subjectRepository.Add(artificialIntelligence);
                _subjectRepository.Add(informationSecurity);
                returnString += "* Subject added to datebase - Done! \n";

                //ADD professors
                _professorRepository.Add(leonidProf);
                _professorRepository.Add(AStanimirovicProg);
                _professorRepository.Add(MilosBogdanovicProf);
                _professorRepository.Add(vladanProf);
                _professorRepository.Add(rajkoProf);
                _professorRepository.Add(vuckoProf);
                _professorRepository.Add(aksProf);
                returnString += "* Professor  added to datebase - Done! \n";

                //READ students brInd and name from file
                //POST students to database
                listOfProfessors.Add(MilosBogdanovicProf);
                listOfProfessors.Add(vladanProf);
                listOfProfessors.Add(rajkoProf);

                listOfSubjects.Add(advancedDatebases);
                listOfSubjects.Add(artificialIntelligence);
                listOfSubjects.Add(informationSecurity);

                List<Student> students = new List<Student>();
                using (var fs = new FileStream("students.txt", FileMode.Open, FileAccess.Read))
                { 
                    using (var sr = new System.IO.StreamReader(fs))
                    {
                        string line = null;
                        char group = 'A';
                        int counterForGroup = 0;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] brIndNameString = line.Split(',');
                            Student studentTemp = new Student()
                            {
                                BrInd = brIndNameString[0],
                                Name = brIndNameString[1]
                            };

                            List<string> generatedLaboratoryExercise = generateLaboratoryExercise(group, studentTemp.BrInd);

                            studentTemp.ListOfLaboratoryExercises = generatedLaboratoryExercise;

                            students.Add(studentTemp);

                            if (counterForGroup++ % 15 == 0)
                                group++;
                        }
                    }
                    returnString += "* Students added to datebase - Done! \n";
                }

                ///add students to datebase
                
                foreach(Student s in students)
                    _studentRepository.Add(s);

                return returnString;
            }

            return "Unknown";
        }

        public List<string> generateLaboratoryExercise(char Group, string brInd)
        {
            List<string> returnList = new List<string>();

            LaboratoryExercise singleLabSub0 = new LaboratoryExercise()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Group = Group,
                ProfessorId = listOfProfessors.ElementAt(0).Id,
                subjectId = listOfSubjects.ElementAt(0).Id,
                studentId = brInd
            };
            List<SingleExercise> generatedSingleExercises0 = generateSingleExercies(
                singleLabSub0.Id,
                brInd,
                listOfSubjects.ElementAt(0).Id,
                listOfProfessors.ElementAt(0).Id
            );
            List<string> listOfId0 = GetIdsOfSingleExercises(generatedSingleExercises0);
            singleLabSub0.ListOfSingleExercise = listOfId0;
            returnList.Add(singleLabSub0.Id);
            _laboratoryExerciseRepository.Add(singleLabSub0);
            listOfProfessors.ElementAt(0).LaboratoryIds.Add(singleLabSub0.Id);
            _professorRepository.Update(listOfProfessors.ElementAt(0).Id, listOfProfessors.ElementAt(0));

            LaboratoryExercise singleLabSub1 = new LaboratoryExercise()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Group = Group,
                ProfessorId = listOfProfessors.ElementAt(1).Id,
                subjectId = listOfSubjects.ElementAt(1).Id,
                studentId = brInd
            };
            List<SingleExercise> generatedSingleExercises1 = generateSingleExercies(
                singleLabSub1.Id,
                brInd,
                listOfSubjects.ElementAt(1).Id,
                listOfProfessors.ElementAt(1).Id
            );
            List<string> listOfId1 = GetIdsOfSingleExercises(generatedSingleExercises1);
            singleLabSub1.ListOfSingleExercise = listOfId1;
            returnList.Add(singleLabSub1.Id);
            _laboratoryExerciseRepository.Add(singleLabSub1);
            listOfProfessors.ElementAt(1).LaboratoryIds.Add(singleLabSub1.Id);
            _professorRepository.Update(listOfProfessors.ElementAt(1).Id, listOfProfessors.ElementAt(1));

            LaboratoryExercise singleLabSub2 = new LaboratoryExercise()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Group = Group,
                ProfessorId = listOfProfessors.ElementAt(2).Id,
                subjectId = listOfSubjects.ElementAt(2).Id,
                studentId = brInd
            };
            List<SingleExercise> generatedSingleExercises2 = generateSingleExercies(
                singleLabSub2.Id,
                brInd,
                listOfSubjects.ElementAt(2).Id,
                listOfProfessors.ElementAt(2).Id
            );
            List<string> listOfId2 = GetIdsOfSingleExercises(generatedSingleExercises2);
            singleLabSub2.ListOfSingleExercise = listOfId2;
            returnList.Add(singleLabSub2.Id);
            _laboratoryExerciseRepository.Add(singleLabSub2);
            listOfProfessors.ElementAt(2).LaboratoryIds.Add(singleLabSub2.Id);
            _professorRepository.Update(listOfProfessors.ElementAt(2).Id, listOfProfessors.ElementAt(2));

            return returnList;
        }

        public List<SingleExercise> generateSingleExercies(string laboratoryExeID, string studentID, 
                                                           string subjectId, string ProfessorId)
        {
            Random rnd = new Random();
            List<SingleExercise> returnList = new List<SingleExercise>();
            for(int indexOfExercise = 0; indexOfExercise < 7; indexOfExercise++)
            {
                SingleExercise singleExe = new SingleExercise()
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    laboratoryExeID = laboratoryExeID,
                    studentId = studentID,
                    subjectId = subjectId,
                    ProfessorId = ProfessorId,
                    OrdinalNumber = indexOfExercise,
                    Comment = "Test comment" };
                if(0.5 > rnd.NextDouble())
                {
                    singleExe.Presence = true;
                    int numOfPoints = rnd.Next(1, 10);
                    singleExe.PointsNumber = numOfPoints;
                }
                else
                {
                    singleExe.Presence = false;
                    singleExe.PointsNumber = 0;
                }
                _singleExerciseRepository.Add(singleExe);
                returnList.Add(singleExe);
            }

            return returnList;
        }


        private List<string> GetIdsOfSingleExercises(List<SingleExercise> generatedSingleExercises)
        {
            List<string> listOfIdsSingleExercise = new List<string>();
            foreach(SingleExercise singExes in generatedSingleExercises)
            {
                listOfIdsSingleExercise.Add(singExes.Id);
            }

            return listOfIdsSingleExercise;
        }
    }
}