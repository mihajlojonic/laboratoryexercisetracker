using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LaboratoryExerciseTrackerWebAPI.Model;
using LaboratoryExerciseTrackerWebAPI.Interfaces;
using MongoDB.Bson;
using LaboratoryExerciseTrackerWebAPI.Data;

namespace LaboratoryExerciseTrackerWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProfessorController : Controller
    {
        private readonly IRepository<Professor> _professorRepository;

        public ProfessorController(IRepository<Professor> professorRepository)
        {
            _professorRepository = professorRepository;
        }

        [HttpGet]
        public Task<List<Professor>> Get()
        {
            return GetProfessorInternal();
        }

        private Task<List<Professor>> GetProfessorInternal()
        {
            return _professorRepository.GetAll();
        }

        //GET api/professor/5
        [HttpGet("{id}")]
        public Task<Professor> Get(string id)
        {
            return GetProfessorByIdInternal(id);
        }

        private async Task<Professor> GetProfessorByIdInternal(string id)
        {
            return await _professorRepository.Get(id) ?? new Professor();
        }

        //POST api/professor
        [HttpPost]
        public void Post([FromBody]List<string> subjectsIds, [FromBody]List<string> laboratoryIds, string name)
        {
            _professorRepository.Add(new Professor()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Name = name,
                SubjectsIds = subjectsIds,
                LaboratoryIds = laboratoryIds
            });
        }

        // PUT api/professor/5
        [HttpPut("{id}")]
        public async void Put(string id, Professor professors)
        {
            await (_professorRepository as ProfessorRepository).Update(id, professors);
        }

        // DELETE api/professor/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _professorRepository.Remove(id);
        }
    }
}