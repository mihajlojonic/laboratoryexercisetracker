using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LaboratoryExerciseTrackerWebAPI.Data;
using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.AspNetCore.Mvc;

namespace LaboratoryExerciseTrackerWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class SingleExerciseController : Controller
    {
        private readonly IRepository<SingleExercise> _singleExerciseRepository;

        public SingleExerciseController(IRepository<SingleExercise> singleExerciseRepository)
        {
            _singleExerciseRepository = singleExerciseRepository;
        }

        [HttpGet]
        public Task<List<SingleExercise>> Get()
        {
            return GetSingleExerciseInternal();
        }

        private Task<List<SingleExercise>> GetSingleExerciseInternal()
        {
            return _singleExerciseRepository.GetAll();
        }

        //GET api/singleexercise/5
        [HttpGet("{id}")]
        public Task<SingleExercise> Get(string id)
        {
            return GetSingleExerciseByIdInternal(id);
        }

        private async Task<SingleExercise> GetSingleExerciseByIdInternal(string id)
        {
            return await _singleExerciseRepository.Get(id) ?? new SingleExercise();
        }

        //POST api/singleexercise
        [HttpPost]
        public void Post(SingleExercise newSingleExercise)
        {
            _singleExerciseRepository.Add(newSingleExercise);
        }

        // PUT api/singleexercise/5
        [HttpPut("{id}")]
        public async void Put(string id, SingleExercise singleExercise)
        {
            await (_singleExerciseRepository as SingleExerciseRepository).Update(id, singleExercise);
        }

        // DELETE api/singleexercise/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _singleExerciseRepository.Remove(id);
        }
    }
}