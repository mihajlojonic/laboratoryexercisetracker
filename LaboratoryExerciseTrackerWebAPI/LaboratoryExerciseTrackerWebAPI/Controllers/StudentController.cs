using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LaboratoryExerciseTrackerWebAPI.Data;
using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.AspNetCore.Mvc;

namespace LaboratoryExerciseTrackerWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private readonly IRepository<Student> _studentRepository;

        public StudentController(IRepository<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }

        [HttpGet]
        public Task<List<Student>> Get()
        {
            return GetProfessorInternal();
        }

        private Task<List<Student>> GetProfessorInternal()
        {
            return _studentRepository.GetAll();
        }

        //GET api/student/5
        [HttpGet("{id}")]
        public Task<Student> Get(string id)
        {
            return GetStudentByIdInternal(id);
        }

        private async Task<Student> GetStudentByIdInternal(string id)
        {
            return await _studentRepository.Get(id) ?? new Student();
        }

        //POST api/student
        [HttpPost]
        public void Post(Student newStudent)
        {
            _studentRepository.Add(newStudent);
        }

        // PUT api/student/5
        [HttpPut("{id}")]
        public async void Put(string id, Student student)
        {
            await (_studentRepository as StudentRepository).Update(id, student);
        }

        // DELETE api/student/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _studentRepository.Remove(id);
        }
    }
}