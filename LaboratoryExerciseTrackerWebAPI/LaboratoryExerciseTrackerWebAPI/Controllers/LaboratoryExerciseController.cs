using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LaboratoryExerciseTrackerWebAPI.Data;
using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using Microsoft.AspNetCore.Mvc;

namespace LaboratoryExerciseTrackerWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class LaboratoryExerciseController : Controller
    {
        private readonly IRepository<LaboratoryExercise> _laboratoryExerciseRepository;

        public LaboratoryExerciseController(IRepository<LaboratoryExercise> laboratoryExerciseRepository)
        {
            _laboratoryExerciseRepository = laboratoryExerciseRepository;
        }

        [HttpGet]
        public Task<List<LaboratoryExercise>> Get()
        {
            return GetLaboratoryExerciseInternal();
        }

        private Task<List<LaboratoryExercise>> GetLaboratoryExerciseInternal()
        {
            return _laboratoryExerciseRepository.GetAll();
        }

        //GET api/laboratoryexercise/5
        [HttpGet("{id}")]
        public Task<LaboratoryExercise> Get(string id)
        {
            return GetLaboratoryExerciseByIdInternal(id);
        }

        private async Task<LaboratoryExercise> GetLaboratoryExerciseByIdInternal(string id)
        {
            return await _laboratoryExerciseRepository.Get(id) ?? new LaboratoryExercise();
        }

        //POST api/laboratoryexercise
        [HttpPost]
        public void Post(LaboratoryExercise newLaboratoryExercise)
        {
            _laboratoryExerciseRepository.Add(newLaboratoryExercise);
        }

        // PUT api/laboratoryexercise/5
        [HttpPut("{id}")]
        public async void Put(string id, LaboratoryExercise laboratoryExercise)
        {
            await (_laboratoryExerciseRepository as LaboratoryExerciseRepository).Update(id, laboratoryExercise);
        }

        // DELETE api/laboratoryexercise/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _laboratoryExerciseRepository.Remove(id);
        }
    }
}