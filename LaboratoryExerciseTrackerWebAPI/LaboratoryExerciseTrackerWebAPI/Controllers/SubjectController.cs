using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LaboratoryExerciseTrackerWebAPI.Interfaces;
using LaboratoryExerciseTrackerWebAPI.Model;
using LaboratoryExerciseTrackerWebAPI.Data;
using MongoDB.Bson;

namespace LaboratoryExerciseTrackerWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class SubjectController : Controller
    {
        private readonly IRepository<Subject> _subjectRepository;

        public SubjectController(IRepository<Subject> subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        [HttpGet]
        public Task<List<Subject>> Get()
        {
            return GetSubjectInternal();
        }
        
        private Task<List<Subject>> GetSubjectInternal()
        {
            return _subjectRepository.GetAll();
        }

        //GET api/subject/5
        [HttpGet("{id}")]
        public Task<Subject> Get(string id)
        {
            return GetSubjectByIdInternal(id);
        }

        private async Task<Subject> GetSubjectByIdInternal(string id)
        {
            return await _subjectRepository.Get(id) ?? new Subject();
        }

        //POST api/subject
        [HttpPost]
        public void Post([FromBody]IDictionary<string, string> professors, string name)
        {
            _subjectRepository.Add(new Subject()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Name = name,
                Professors = professors

            });
        }

        // PUT api/subject/5
        [HttpPut("{id}")]
        public async void Put(string id, [FromBody]IDictionary<string, string> professors)
        {
            await (_subjectRepository as SubjectRepository).Update(id, professors);
        }

        // DELETE api/subject/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _subjectRepository.Remove(id);
        }
    }
}