﻿namespace LabaratoryExerciseTrackerFormApp
{
    partial class FormExamples
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGetAllSingleExercise = new System.Windows.Forms.Button();
            this.btnGetAllLaboratoryExercise = new System.Windows.Forms.Button();
            this.btnGetAllStudent = new System.Windows.Forms.Button();
            this.btnGetAllSubject = new System.Windows.Forms.Button();
            this.btnGetAllProfessor = new System.Windows.Forms.Button();
            this.textJSONoutput = new System.Windows.Forms.TextBox();
            this.lblJSONoutputConsole = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGetAllProfessor);
            this.groupBox1.Controls.Add(this.btnGetAllSubject);
            this.groupBox1.Controls.Add(this.btnGetAllStudent);
            this.groupBox1.Controls.Add(this.btnGetAllLaboratoryExercise);
            this.groupBox1.Controls.Add(this.btnGetAllSingleExercise);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 479);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Request";
            // 
            // btnGetAllSingleExercise
            // 
            this.btnGetAllSingleExercise.Location = new System.Drawing.Point(7, 31);
            this.btnGetAllSingleExercise.Name = "btnGetAllSingleExercise";
            this.btnGetAllSingleExercise.Size = new System.Drawing.Size(187, 70);
            this.btnGetAllSingleExercise.TabIndex = 0;
            this.btnGetAllSingleExercise.Text = "Get All Single Exercise";
            this.btnGetAllSingleExercise.UseVisualStyleBackColor = true;
            this.btnGetAllSingleExercise.Click += new System.EventHandler(this.btnGetAllSingleExercise_Click);
            // 
            // btnGetAllLaboratoryExercise
            // 
            this.btnGetAllLaboratoryExercise.Location = new System.Drawing.Point(7, 119);
            this.btnGetAllLaboratoryExercise.Name = "btnGetAllLaboratoryExercise";
            this.btnGetAllLaboratoryExercise.Size = new System.Drawing.Size(187, 70);
            this.btnGetAllLaboratoryExercise.TabIndex = 1;
            this.btnGetAllLaboratoryExercise.Text = "Get All Laboratory Exercise";
            this.btnGetAllLaboratoryExercise.UseVisualStyleBackColor = true;
            this.btnGetAllLaboratoryExercise.Click += new System.EventHandler(this.btnGetAllLaboratoryExercise_Click);
            // 
            // btnGetAllStudent
            // 
            this.btnGetAllStudent.Location = new System.Drawing.Point(7, 214);
            this.btnGetAllStudent.Name = "btnGetAllStudent";
            this.btnGetAllStudent.Size = new System.Drawing.Size(187, 70);
            this.btnGetAllStudent.TabIndex = 2;
            this.btnGetAllStudent.Text = "Get All Students";
            this.btnGetAllStudent.UseVisualStyleBackColor = true;
            this.btnGetAllStudent.Click += new System.EventHandler(this.btnGetAllStudent_Click);
            // 
            // btnGetAllSubject
            // 
            this.btnGetAllSubject.Location = new System.Drawing.Point(7, 310);
            this.btnGetAllSubject.Name = "btnGetAllSubject";
            this.btnGetAllSubject.Size = new System.Drawing.Size(187, 70);
            this.btnGetAllSubject.TabIndex = 3;
            this.btnGetAllSubject.Text = "Get All Subjects";
            this.btnGetAllSubject.UseVisualStyleBackColor = true;
            this.btnGetAllSubject.Click += new System.EventHandler(this.btnGetAllSubject_Click);
            // 
            // btnGetAllProfessor
            // 
            this.btnGetAllProfessor.Location = new System.Drawing.Point(7, 403);
            this.btnGetAllProfessor.Name = "btnGetAllProfessor";
            this.btnGetAllProfessor.Size = new System.Drawing.Size(187, 70);
            this.btnGetAllProfessor.TabIndex = 4;
            this.btnGetAllProfessor.Text = "Get All Professors";
            this.btnGetAllProfessor.UseVisualStyleBackColor = true;
            this.btnGetAllProfessor.Click += new System.EventHandler(this.btnGetAllProfessor_Click);
            // 
            // textJSONoutput
            // 
            this.textJSONoutput.Location = new System.Drawing.Point(219, 44);
            this.textJSONoutput.Multiline = true;
            this.textJSONoutput.Name = "textJSONoutput";
            this.textJSONoutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textJSONoutput.Size = new System.Drawing.Size(750, 442);
            this.textJSONoutput.TabIndex = 1;
            // 
            // lblJSONoutputConsole
            // 
            this.lblJSONoutputConsole.AutoSize = true;
            this.lblJSONoutputConsole.Location = new System.Drawing.Point(220, 21);
            this.lblJSONoutputConsole.Name = "lblJSONoutputConsole";
            this.lblJSONoutputConsole.Size = new System.Drawing.Size(93, 17);
            this.lblJSONoutputConsole.TabIndex = 2;
            this.lblJSONoutputConsole.Text = "JSON output:";
            // 
            // FormExamples
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 504);
            this.Controls.Add(this.lblJSONoutputConsole);
            this.Controls.Add(this.textJSONoutput);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormExamples";
            this.Text = "FormExamples";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGetAllProfessor;
        private System.Windows.Forms.Button btnGetAllSubject;
        private System.Windows.Forms.Button btnGetAllStudent;
        private System.Windows.Forms.Button btnGetAllLaboratoryExercise;
        private System.Windows.Forms.Button btnGetAllSingleExercise;
        private System.Windows.Forms.TextBox textJSONoutput;
        private System.Windows.Forms.Label lblJSONoutputConsole;
    }
}