﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabaratoryExerciseTrackerFormApp.Model
{
    public class SingleExercise
    {
        public string Id { get; set; }
        public string laboratoryExeID { get; set; }
        public string studentId { get; set; }
        public string subjectId { get; set; }
        public string ProfessorId { get; set; }
        public int OrdinalNumber { get; set; }
        public bool Presence { get; set; } = false;
        public int PointsNumber { get; set; } = 0;
        public string Comment { get; set; }
    }
}
