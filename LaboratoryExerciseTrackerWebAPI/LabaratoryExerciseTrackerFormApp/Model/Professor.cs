﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabaratoryExerciseTrackerFormApp.Model
{
    public class Professor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<string> Subjects { get; set; }
        public List<string> Laboratory { get; set; }
    }
}
