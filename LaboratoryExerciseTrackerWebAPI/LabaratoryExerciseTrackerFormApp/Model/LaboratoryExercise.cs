﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabaratoryExerciseTrackerFormApp.Model
{
    public class LaboratoryExercise
    {
        public string Id { get; set; }
        public string studentId { get; set; }
        public string subjectId { get; set; }
        public string ProfessorId { get; set; }
        public char Group { get; set; }
        public List<string> ListOfSingleExercise { get; set; }
    }
}
