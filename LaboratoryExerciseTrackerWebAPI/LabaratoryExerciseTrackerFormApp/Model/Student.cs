﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabaratoryExerciseTrackerFormApp.Model
{
    public class Student
    {
        public string BrInd { get; set; }
        public string Name { get; set; }
        public List<string> ListOfLaboratoryExercises { get; set; } 
    }
}
