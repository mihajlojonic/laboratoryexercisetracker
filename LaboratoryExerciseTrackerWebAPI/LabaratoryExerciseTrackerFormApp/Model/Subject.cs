﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabaratoryExerciseTrackerFormApp.Model
{
    public class Subject
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IDictionary<string, string> Professors { get; set; }
    }
}
