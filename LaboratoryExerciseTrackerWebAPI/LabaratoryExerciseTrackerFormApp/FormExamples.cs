﻿using LabaratoryExerciseTrackerFormApp.Data;
using LabaratoryExerciseTrackerFormApp.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabaratoryExerciseTrackerFormApp
{
    public partial class FormExamples : Form
    {
        public FormExamples()
        {
            InitializeComponent();
        }

        private void btnGetAllSingleExercise_Click(object sender, EventArgs e)
        {
            textJSONoutput.Text = "";
            var exercises = DataProvider.GetAllSingleExercise();

            exercises.ContinueWith(exe =>
            {
                var exes = exe.Result;
                int i = 0;
                foreach (var ex in exes)
                {
                    ShowSingleExercise(ex as SingleExercise);
                    if (i > 50) break;
                    i++;
                }
            },
                TaskContinuationOptions.OnlyOnRanToCompletion
            );
        }

        private void btnGetAllSubject_Click(object sender, EventArgs e)
        {
            textJSONoutput.Text = "";
            var exercises = DataProvider.GetAllSubjects();

            exercises.ContinueWith(sub =>
            {
                var subs = sub.Result;
                int i = 0;
                foreach (var su in subs)
                {
                    ShowSubject(su as Subject);
                    if (i > 50) break;
                    i++;
                }
            },
                TaskContinuationOptions.OnlyOnRanToCompletion
            );
        }

        private void btnGetAllLaboratoryExercise_Click(object sender, EventArgs e)
        {
            textJSONoutput.Text = "";
            var labExes = DataProvider.GetAllLaboratoryExercise();

            labExes.ContinueWith(sub =>
            {
                var labExesResult = labExes.Result;
                int i = 0;
                foreach (var lxr in labExesResult)
                {
                    ShowLaboratoryExercise(lxr as LaboratoryExercise);
                    if (i > 20) break;
                    i++;
                }
            },
                TaskContinuationOptions.OnlyOnRanToCompletion
            );
        }

        private void btnGetAllStudent_Click(object sender, EventArgs e)
        {
            textJSONoutput.Text = "";
            var students = DataProvider.GetAllStudents();

            students.ContinueWith(sub =>
            {
                var studentsResult = students.Result;
                int i = 0;
                foreach (var lxr in studentsResult)
                {
                    ShowStudent(lxr as Student);
                    if (i > 20) break;
                    i++;
                }
            },
                TaskContinuationOptions.OnlyOnRanToCompletion
            );
        }

        private void btnGetAllProfessor_Click(object sender, EventArgs e)
        {
            textJSONoutput.Text = "";
            var professors = DataProvider.GetAllProfessors();

            professors.ContinueWith(sub =>
            {
                var professorsResult = professors.Result;
                int i = 0;
                foreach (var lxr in professorsResult)
                {
                    ShowProfessor(lxr as Professor);
                    if (i > 20) break;
                    i++;
                }
            },
                TaskContinuationOptions.OnlyOnRanToCompletion
            );
        }

        private void ShowSingleExercise(SingleExercise singleExercise)
        {
            textJSONoutput.Text += $"Id: {singleExercise.Id}\tLabaratoryExeID: {singleExercise.laboratoryExeID}" +
                              $"StudentId: {singleExercise.studentId}\tSubjectId: {singleExercise.subjectId}" +
                              $"ProfessorId: {singleExercise.ProfessorId}\tOrdinalNumber: {singleExercise.OrdinalNumber}" +
                              $"Presence: {singleExercise.Presence}\tPointsNumber: {singleExercise.PointsNumber}" +
                              $"Comment: {singleExercise.Comment}" + Environment.NewLine;
        }

        private void ShowSubject(Subject subject)
        {
            textJSONoutput.Text += $"Id: {subject.Id}\tName: {subject.Name}" + Environment.NewLine;

            textJSONoutput.Text += "Professors: " + Environment.NewLine;
            foreach(KeyValuePair<string,string> kvp in subject.Professors)
            {
                textJSONoutput.Text += "--- " + kvp.Key + ", " + kvp.Value.ToString();
            }
        }

        private void ShowLaboratoryExercise(LaboratoryExercise labExe)
        {
            textJSONoutput.Text += $"Id: {labExe.Id}\tStudentId: {labExe.studentId}" +
                                   $"subjectId: {labExe.subjectId}\tProfessorId: {labExe.ProfessorId}" +
                                   $"Group: {labExe.Group}\t"
                                 + Environment.NewLine;

            textJSONoutput.Text += "List of Single Exercise: " + Environment.NewLine;
            foreach (string los in labExe.ListOfSingleExercise)
            {
                textJSONoutput.Text += "--- " + los + Environment.NewLine;
            }
        }

        private void ShowStudent(Student student)
        {
            textJSONoutput.Text += $"Id: {student.BrInd}\tName: {student.Name}" 
                                 + Environment.NewLine;

            textJSONoutput.Text += "List of Laboraotry Exercises: " + Environment.NewLine;
            foreach (string los in student.ListOfLaboratoryExercises)
            {
                textJSONoutput.Text += "--- " + los + Environment.NewLine;
            }
        }

        private void ShowProfessor(Professor professor)
        {
            textJSONoutput.Text += $"Id: {professor.Id}\tName: {professor.Name}"
                                 + Environment.NewLine;

            textJSONoutput.Text += "List of Subjects: " + Environment.NewLine;
            foreach (string los in professor.Subjects)
            {
                textJSONoutput.Text += "--- " + los + Environment.NewLine;
            }

            textJSONoutput.Text += Environment.NewLine;

            textJSONoutput.Text += "List of Laboratories: " + Environment.NewLine;
            foreach (string los in professor.Laboratory)
            {
                textJSONoutput.Text += "--- " + los + Environment.NewLine;
            }
        }

        
    }
}
