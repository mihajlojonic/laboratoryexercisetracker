﻿using LabaratoryExerciseTrackerFormApp.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LabaratoryExerciseTrackerFormApp.Data
{
    public class DataProvider
    {
        public static string baseAddress = "http://localhost:53617/";

        public static async Task<List<SingleExercise>> GetAllSingleExercise()
        {
            var singleExercises = new List<SingleExercise>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage ressponseMessage = await client.GetAsync("api/singleexercise");
                ressponseMessage.EnsureSuccessStatusCode();
                if(ressponseMessage.IsSuccessStatusCode)
                {
                    var stringResult = await ressponseMessage.Content.ReadAsStringAsync();
                    singleExercises = JsonConvert.DeserializeObject<List<SingleExercise>>(stringResult);
                }
            }

            return singleExercises;
        }

        public static async Task<List<Subject>> GetAllSubjects()
        {
            var subjects = new List<Subject>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage responseMessage = await client.GetAsync("api/subject");
                responseMessage.EnsureSuccessStatusCode();
                if(responseMessage.IsSuccessStatusCode)
                {
                    var stringResult = await responseMessage.Content.ReadAsStringAsync();
                    subjects = JsonConvert.DeserializeObject<List<Subject>>(stringResult);                    
                }
            }

            return subjects;
        }

        public static async Task<List<Professor>> GetAllProfessors()
        {
            var professors = new List<Professor>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage responseMessage = await client.GetAsync("api/professor");
                responseMessage.EnsureSuccessStatusCode();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var stringResult = await responseMessage.Content.ReadAsStringAsync();
                    professors = JsonConvert.DeserializeObject<List<Professor>>(stringResult);
                }
            }

            return professors;
        }

        public static async Task<List<LaboratoryExercise>> GetAllLaboratoryExercise()
        {
            var labExe = new List<LaboratoryExercise>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage responseMessage = await client.GetAsync("api/laboratoryexercise");
                responseMessage.EnsureSuccessStatusCode();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var stringResult = await responseMessage.Content.ReadAsStringAsync();
                    labExe = JsonConvert.DeserializeObject<List<LaboratoryExercise>>(stringResult);
                }
            }

            return labExe;
        }

        public static async Task<List<Student>> GetAllStudents()
        {
            var students = new List<Student>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage responseMessage = await client.GetAsync("api/student");
                responseMessage.EnsureSuccessStatusCode();
                if (responseMessage.IsSuccessStatusCode)
                {
                    var stringResult = await responseMessage.Content.ReadAsStringAsync();
                    students = JsonConvert.DeserializeObject<List<Student>>(stringResult);
                }
            }

            return students;
        }
    }
}
